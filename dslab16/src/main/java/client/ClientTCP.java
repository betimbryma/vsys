package client;

import util.Channel;

import java.io.*;
import java.util.concurrent.Semaphore;

public class ClientTCP implements Runnable{
	
	
	private String msg;
	private String somesMsg;
	private PrintStream userResponseStream;
	private final Semaphore newWord = new Semaphore(1, true);
	private final Semaphore wordRead = new Semaphore(1, true);
	private final Semaphore authenticate = new Semaphore(0, true);
	private boolean open;
	private Channel channel;
	public ClientTCP(Channel channel, PrintStream userResponseStream){
		this.channel=channel;
		this.userResponseStream=userResponseStream;
		open=true;
		msg="no message received!";
	}
	
	public void run(){

		try {
			wordRead.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			//cant do anything, think for later------------------------------------
		}

		try {
			readFromServer();
		} catch (IOException e) {
//			System.out.println("print socket closed");
		}
	}
	
	
	
	public String readServer()  throws IOException {
		if(open){
		try {
			wordRead.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			//cant do anything, think for later------------------------------------
			channel.exit(2);
		}
		String m = somesMsg;
		newWord.release();
	
		return m;
		}else{
			return null;
		}
	}



	public String readFromServer() throws IOException {
		String msg;
		while(true){


			holdRead();
			msg=channel.read();
			releaseRead();


			if(msg==null){
				open=false;
					break;
			}
			if(msg.substring(0,5).equals("reply")){
				try {
					newWord.acquire();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//cant do anything, think for later------------------------------------
				}
				this.somesMsg=msg.substring(5);

				if(somesMsg.equals("Successfully logged out.")){
					holdRead();
				}


				wordRead.release();
				
			}else if(msg.substring(0,5).equals("group")){
				this.msg=msg.substring(5);
				userResponseStream.println(this.msg);
			}else{
				this.somesMsg="Wtf happened";
				this.msg="Wtf happened";
			}
		}
		return null;
	}


	public String readlastMsg(){
		return this.msg;
	}


	public void holdRead(){
		try {
			authenticate.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public void releaseRead(){
		authenticate.release();
	}
	public void exit(){
		releaseRead();
	}
}
