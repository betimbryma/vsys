package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import util.*;
import java.security.MessageDigest;
import javax.crypto.Mac;

public class ClientServer implements Runnable {

	private String address;
	private String port;
	private BufferedReader reader;
	private PrintWriter writer;
	private PrintStream userResponseStream;
	private ServerSocket serverSocket;
	private Socket socket;
	private Config config;
	
	public ClientServer(String address, String port,PrintStream userResponseStream, Config config){
		this.address=address;
		this.port=port;
		this.userResponseStream=userResponseStream;
		this.config = config;
	}
	
	public void run(){
		try {
			 serverSocket = new ServerSocket(Integer.valueOf(port));
	
			 while(true){
				socket=serverSocket.accept();
			 	reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				writer = new PrintWriter(socket.getOutputStream(),true);
				
				String message = reader.readLine();
				String part[] = message.split(" ");

				String content = "";
				for (int i=3; i<part.length; i++){
					content += " " + part[i];
				}
				
				userResponseStream.println(part[1] + " " + part[2] + content);
				
				byte[] computedHMAC;
				
				if(part.length > 3){
				
					byte[] receivedHMAC = part[0].getBytes();
					
					computedHMAC = HMac.getHMAC(part[1] + content, config);
					
					if(MessageDigest.isEqual(receivedHMAC, computedHMAC)){
						writer.println(new String(HMac.getHMAC("!ack", config)) + " !ack");
					} else {
//						computedHMAC = HMac.getWrongHMAC("!tampered" + content, config);
						computedHMAC = HMac.getHMAC("!tampered" + content, config);
						writer.println(new String(computedHMAC) + " !tampered" + content);
					}
					
				} else {
					computedHMAC = HMac.getHMAC("!tampered incomplete message", config);
					writer.println(new String(computedHMAC) + " !tampered incomplete message");
				}
			 }
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			//I don't know what exception to output!
		}finally{
			exit();
		}
		

		
	}
	
	public void exit(){
		try {
			if(serverSocket!=null&&!serverSocket.isClosed())
				serverSocket.close();
			if(socket!=null&&!socket.isClosed())
				socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//cant do anything
		}

		if(writer!=null)
			writer.close();

		if(reader!=null)
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
}
