package client;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import org.bouncycastle.util.encoders.Base64;
import util.*;
import java.security.MessageDigest;
import javax.crypto.Mac;

public class Client implements IClientCli, Runnable {

	private String componentName;
	private Config config;
	private InputStream userRequestStream;
	private PrintStream userResponseStream;
	private PrintWriter serverWriter;
	private Socket socket=null;
	private ClientTCP clientTCP;
	private String user;
	private ClientServer clientserver;
    private Channel channel;
	private boolean authenticated;
    private Socket socketMSG=null;
	private BufferedReader serverReaderMSG=null;
	private PrintWriter serverWriterMSG=null;

	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public Client(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream) {
		this.componentName = componentName;
		this.config = config;
		this.userRequestStream = userRequestStream;
		this.userResponseStream = userResponseStream;
		// TODO
	}

	@Override
	public void run() {
		// TODO

		BufferedReader serverReader=null;
		BufferedReader userInputReader = new BufferedReader(new InputStreamReader(userRequestStream));

		try {
			socket = new Socket(config.getString("chatserver.host"),config.getInt("chatserver.tcp.port"));
			serverReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			serverWriter = new PrintWriter(socket.getOutputStream(), true);
			channel = new Base64Channel(new TcpChannel(serverReader,userResponseStream,serverWriter));
			clientTCP = new ClientTCP(channel, userResponseStream);
			new Thread(clientTCP).start();
			authenticated=false;
			while(true){
				String response="Not a valid Command!";
				String input=userInputReader.readLine();
				String[] requestArray = input.split(" ");
				if(requestArray.length<1){
					continue;
				}
				if(requestArray[0].equals("!authenticate")){
					if(requestArray.length==2){
						response=authenticate(requestArray[1]);
					}
				}else if(authenticated==false&&!requestArray[0].equals("!exit")){
					userResponseStream.println("You are not authenticated!");
					continue;
				}
				if(requestArray[0].equals("!login")){
					response="Login is not anymore a valid command!";
					if(requestArray.length==3&&false==true){
						response=login(requestArray[1],requestArray[2]);
					}
				}
				if(requestArray[0].equals("!logout")){
						response=logout();
				}
				if(requestArray[0].equals("!send")){
					String[] requestArray2=input.split("!send |");
					String message="";
					for(int i =1;i<requestArray2.length;i++){
						message+=requestArray2[i];
					}
					response=send(message);
				}
				if(requestArray[0].equals("!register")){
					if(requestArray.length==2){
						response=register(requestArray[1]);
					}
				}
				if(requestArray[0].equals("!lookup")){
					if(requestArray.length==2){
						response=lookup(requestArray[1]);
					}
				}
				//TODO remove white space at the end of the message
				if(requestArray[0].equals("!msg")){
					if(!(requestArray.length<3)){
						String s="";
						int i =2;
						for(;i<requestArray.length-1;i++)
							s+=requestArray[i]+" ";
						s+=requestArray[i];
						response=msg(requestArray[1],s);

					}
				}
				if(requestArray[0].equals("!list")){
					if(requestArray.length==1){
						response=list();
					}
				}
				if(requestArray[0].equals("!lastMsg")){
					if(requestArray.length==1){
						response=lastMsg();
					}
				}
				if(requestArray[0].equals("!exit")){
					if(requestArray.length==1){
						response=exit();
						userResponseStream.println(response);
						break;
					}
				}

				userResponseStream.println(response);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			if(serverReaderMSG!=null)
				try {
					serverReaderMSG.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			if(serverWriterMSG!=null)
				serverWriterMSG.close();
			if(socketMSG!=null&&!socketMSG.isClosed())
				try {
					socketMSG.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			socketMSG=null;
			serverReaderMSG=null;
			serverWriterMSG=null;
			userResponseStream.println(e.getMessage());
			exit();
		}

	}

	@Override
	public String login(String username, String password) throws IOException {
		if(channel.isPrintOpen()){
            channel.write("!login "+username+" "+password);
			user=username;
			return clientTCP.readServer();
		}

		return null;
	}

	@Override
	public String logout() throws IOException {
		// TODO Auto-generated method stub

		if(channel!=null) {
			if (channel.isPrintOpen()) {
				channel.write("!logout");
				String msg = clientTCP.readServer();
				if(msg.equals("Successfully logged out.")){
					authenticated=false;
				}
				return msg;
			}
		}

		return null;
	}

	@Override
	public String send(String message) throws IOException {
		// TODO Auto-generated method stub
        if(channel.isPrintOpen()){
            channel.write("!send "+message);
			return clientTCP.readServer();
		}
		return null;
	}

	@Override
	public String list() throws IOException {
		// TODO Auto-generated method stub
		DatagramSocket socket = null;
		BufferedReader userInputReader = null;
		byte[] buffer;
		DatagramPacket packet;




		int port= config.getInt("chatserver.udp.port");

			socket = new DatagramSocket();
			String input = "!list";
			buffer = input.getBytes();
			packet = new DatagramPacket(buffer, buffer.length,
					InetAddress.getByName(config.getString("chatserver.host")),
					config.getInt("chatserver.udp.port"));
			socket.send(packet);
			buffer = new byte[1024];
			packet = new DatagramPacket(buffer, buffer.length);
			socket.receive(packet);
			return new String(packet.getData());


	}

	@Override
	public String msg(String username, String message) throws IOException {
		// TODO Auto-generated method stub
		String m="Wrong username or user not reachable.";
		String address = lookup(username);

		if(address.equals("Not logged in."))
			return address;
		String[] add= address.split(":");
		if(add.length!=2)return address ;
		socketMSG = new Socket(add[0],Integer.valueOf(add[1]));
		serverReaderMSG = new BufferedReader(new InputStreamReader(socketMSG.getInputStream()));
		serverWriterMSG = new PrintWriter(socketMSG.getOutputStream(), true);

		byte[] hmac = HMac.getHMAC("!msg " + message, config);
//		byte[] wronghmac = HMac.getWrongHMAC("!msg " + message, config);

		serverWriterMSG.println(new String(hmac) + " !msg " + user + " " + message);
//		serverWriterMSG.println(new String(wronghmac) + " !msg " + user + " " + message);

		
		String s=null;
		if((s=serverReaderMSG.readLine())!=null){
			String part[] = s.split(" ");
			byte[] computedHMAC;
			byte[] receivedHMAC = part[0].getBytes();
		
			if(part.length > 1){
				
				String content = part[1];
				for (int i=2; i<part.length; i++){
					content += " " + part[i];
				}
				computedHMAC = HMac.getHMAC(content, config);
				
				if(MessageDigest.isEqual(receivedHMAC, computedHMAC)){
					m = username+" replied with "+content;
				} else {
					m = "Response message from " + username + " was tampered with";
				}
				
			} else {
				m = "Response message from " + username + " was tampered with";
			}
		}
		
		if(serverReaderMSG!=null)
			serverReaderMSG.close();
		if(serverWriterMSG!=null)
			serverWriterMSG.close();
		if(socketMSG!=null&&!socketMSG.isClosed())
			socketMSG.close();

		socketMSG=null;
		serverReaderMSG=null;
		serverWriterMSG=null;


		return m;
	}

	@Override
	public String lookup(String username) throws IOException {
		// TODO Auto-generated method stub

        if(channel.isPrintOpen()){
            channel.write("!lookup "+username);
			return clientTCP.readServer();
		}
		return null;
	}

	@Override
	public String register(String privateAddress) throws IOException {
		// TODO Auto-generated method stub

		if(clientserver!=null){
			clientserver.exit();
		}

        if(channel.isPrintOpen()){
            channel.write("!register "+privateAddress);
			String[] add= privateAddress.split(":");
			if(add.length==2){
				clientserver=new ClientServer(add[0],add[1],userResponseStream, config);
				new Thread(clientserver).start();
			}
			return clientTCP.readServer();
		}
		return null;
	}

	@Override
	public String lastMsg() throws IOException {
		// TODO Auto-generated method stub
			return clientTCP.readlastMsg();

	}

	@Override
	public String exit() {
		// TODO Auto-generated method stub
		try {
			if(authenticated)
				logout();
		} catch (IOException e) {
			// TODO Auto-generated catch block

		}
		//serverWriter.println("!exit")
		if (socket != null && !socket.isClosed()){
			try {
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block

			}
		}

		if(clientserver!=null)
			clientserver.exit();

		if(channel!=null) {
			if (channel.isPrintOpen()) {
				this.serverWriter.close();
			}
		}if(clientTCP!=null)
			clientTCP.exit();
		return "Client exit successful";

	}

	/**
	 * @param args
	 *            the first argument is the name of the {@link Client} component
	 */
	public static void main(String[] args) {
		Client client = new Client(args[0], new Config("client"), System.in,System.out);

		new Thread(client).start();
		// TODO: start the client



    }

	// --- Commands needed for Lab 2. Please note that you do not have to
	// implement them for the first submission. ---

	@Override
	public String authenticate(String username) throws IOException {
		// TODO Auto-generated method stub
		String message;
        if((message=channel.authenticateClient(username, config))!=null) {
			if(message.equals("Successfully Authenticated")) {
				authenticated=true;
				user=username;
				clientTCP.releaseRead();
				return message;
			}
        }
		return "not Authenticated";
	}

}
