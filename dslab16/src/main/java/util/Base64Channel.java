package util;

import org.bouncycastle.util.encoders.Base64;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.IOException;
import java.security.*;
import java.util.Set;

/**
 * Created by eni on 08.01.2017.
 */
public class Base64Channel implements Channel{


    private Channel channel;

    private SecretKey secretKey;
    private String paramIV;
    private PublicKey publicKey;
    private PrivateKey privateKey;
    private String challengeForTheOther;
    private String challengeFromTheOther;

    public Base64Channel(Channel channel) {
        this.channel = channel;
    }

    public String authenticateServer(Config config) throws IOException {


        File f = new File(config.getString("key"));
        if (f == null) {
            printlnOut("File not found");
            return null;
        }
        privateKey = null;
        try {
            privateKey = Keys.readPrivatePEM(f);
        } catch (Exception e) {
            printlnOut("IOException at Keys.readPublicPEM(f)");
            return null;
        }

        String endMessage;
        if ((endMessage = readAuthentificate()) != null) {



            String[] requestArray = endMessage.split(" ");
            String challengeFromClient = requestArray[2];

            SecureRandom secureRandom = new SecureRandom();
            byte[] number = new byte[32];
            secureRandom.nextBytes(number);
            challengeForTheOther = new String(Base64.encode(number));

            String sk = new String();
            try {
                KeyGenerator generator = KeyGenerator.getInstance("AES");
                generator.init(256);
                secretKey = generator.generateKey();
                sk = new String(Base64.encode(secretKey.getEncoded()));
            } catch (NoSuchAlgorithmException e) {
                printlnOut("NoSuchAlgorithmException");
            }

            number = new byte[16];
            secureRandom.nextBytes(number);
            paramIV = new String(Base64.encode(number));

            String ok = "!ok" + " " + challengeFromClient + " " + challengeForTheOther + " " + sk + " " + paramIV;
            String path = config.getString("keys.dir") + "/" + requestArray[1] + ".pub.pem";

            f = new File(path);
            if (f == null) {
                printlnOut("File not found");
                return null;
            }
            try {
                publicKey = Keys.readPublicPEM(f);
            } catch (IOException e) {
                printlnOut("IOException at Keys.readPublicPEM(f)");
                return null;
            }

            if( writeAuthenticate(ok)) {
                if(read().equals(challengeForTheOther)){
                    return requestArray[1];
                }
            }
        }
        printlnOut("Authentificate not successfull");
        return null;
    }

    public String authenticateClient(String username, Config config) throws IOException {

        File f = new File(config.getString("chatserver.key"));
        if (f == null) {
            printlnOut("File not found for server public key");
            return null;
        }
        try {
            publicKey = Keys.readPublicPEM(f);
        } catch (IOException e) {
            printlnOut("IOException at Keys.readPublicPEM(f) ");
            return null;
        }
        f = new File(config.getString("keys.dir") + "/" + username + ".pem");
        if (f == null) {
            printlnOut("File not found for private key");
            return null;
        }
        try {
            privateKey = Keys.readPrivatePEM(f);
        } catch (IOException e) {
            printlnOut("IOException at Keys.readPrivatePEM(f)");
            return null;
        }



        SecureRandom secureRandom = new SecureRandom();
        final byte[] number = new byte[32];
        secureRandom.nextBytes(number);
        challengeForTheOther = new String(Base64.encode(number));
        String msg = "!authenticate " + username + " " + challengeForTheOther;


        if (writeAuthenticate(msg)) {
            if (checkResponseFromServer(readAuthentificate())) {
                write(challengeFromTheOther);
                return read();
            }
        }
        return null;
    }


    private boolean checkResponseFromServer(String response) {
        if (response == null) {
            return false;
        }
        String[] responseList = response.split(" ");
        if (responseList.length != 5) {
            printlnOut("There are more arguments in response");
            return false;
        }
        if (!responseList[0].equals("!ok")) {
            printlnOut("Message doesn't beginn with !ok ");
            return false;
        }
        if (!responseList[1].equals(challengeForTheOther)) {
            printlnOut("Server didn't solve the challenge");
            return false;
        }
        challengeFromTheOther=responseList[2];

        byte[] encodedKey     = Base64.decode( responseList[3].getBytes());
        secretKey = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
        paramIV = responseList[4];
        return true;

    }

    private boolean writeAuthenticate(String string){
        Cipher c;
        try {
            c = Cipher.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding");
            try {
                c.init(Cipher.ENCRYPT_MODE, publicKey);
                try {
                    byte[] encryptedMsg = c.doFinal(string.getBytes());
                    String messageToSend = new String(Base64.encode(encryptedMsg));
                    channel.write(messageToSend);
                    return true;
                } catch (IllegalBlockSizeException e) {
                    printlnOut("Illegal Block Size Exception!");
                } catch (BadPaddingException e) {
                    printlnOut("Bad Padding Exception!");
                }
            } catch (InvalidKeyException e) {
                printlnOut("Invalid Key Exception");
            }
        } catch (NoSuchPaddingException e) {
            printlnOut("NoSuch Padding Exception");
        } catch (NoSuchAlgorithmException e) {
            printlnOut("No Such Algorithm Exception");
        }
        return false;
    }

    private String readAuthentificate() throws IOException {
        String message = channel.read();
        byte[] notBase64;
        try {
            notBase64 = Base64.decode(message.getBytes());
            Cipher c;
            try {
                c = Cipher.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding");
                try {
                    c.init(Cipher.DECRYPT_MODE, privateKey);

                    try {
                        byte[] decrypted = c.doFinal(notBase64);
                        return new String(decrypted);
                    } catch (IllegalBlockSizeException e) {
                        printlnOut("IllegalBlockSizeException");
                    } catch (BadPaddingException e) {
                        printlnOut("BadPaddingException");
                    }
                } catch (InvalidKeyException e) {
                    printlnOut("InvalidKeyException");
                }
            } catch (NoSuchAlgorithmException e) {
                printlnOut("NoSuchAlgorithmException");
            } catch (NoSuchPaddingException e) {
                printlnOut("NoSuchPaddingException");
            }
        }catch(NullPointerException e){
            printlnOut("Null Pointer Exception!");
        }
        return null;
    }

    public boolean isPrintOpen() {
        return channel.isPrintOpen();

    }
                                                                                                                // read server
    public String read() throws IOException {
        String msg = channel.read();
        if (msg == null) {
            return msg;
        }
        Cipher c;
        try {
            c = Cipher.getInstance("AES/CTR/NoPadding");
            try {
                c.init(Cipher.DECRYPT_MODE,secretKey, new IvParameterSpec(Base64.decode(paramIV.getBytes())));
                try {

                    byte[] encoded = msg.getBytes();
                    byte[] decodeMessage = Base64.decode(encoded);
                    byte[] decrypted = c.doFinal(decodeMessage);
                    String stringInBytes =  new String(decrypted);
                    return stringInBytes;
                } catch (IllegalBlockSizeException e) {
                    printlnOut("IllegalBlockSizeException");
                } catch (BadPaddingException e) {
                    printlnOut("BadPaddingException");
                }
            } catch (InvalidAlgorithmParameterException e) {
                printlnOut("InvalidAlgorithmParameterException");
            }
        } catch (NoSuchAlgorithmException e) {
            printlnOut("NoSuchAlgorithmException");
        } catch (NoSuchPaddingException e) {
            printlnOut("NoSuchPaddingException");
        } catch (InvalidKeyException e) {
            printlnOut("InvalidKeyException");
        }

        //decode
        return msg;
    }
                                                                                                                                    // write client

    public void write(String string) {
        Cipher c;
        try {
            c = Cipher.getInstance("AES/CTR/NoPadding");
            try {
                c.init(Cipher.ENCRYPT_MODE,secretKey, new IvParameterSpec(Base64.decode(paramIV.getBytes())));
                try {
                    byte[] stringInBytes = string.getBytes();
                    byte[] decoded = Base64.decode(stringInBytes);
                    byte[] encrypted = c.doFinal(stringInBytes);
                    byte[] encode =Base64.encode(encrypted);
                    channel.write(new String(encode));
                } catch (IllegalBlockSizeException e) {
                    printlnOut("IllegalBlockSizeException");
                } catch (BadPaddingException e) {
                    printlnOut("BadPaddingException");
                }
            } catch (InvalidAlgorithmParameterException e) {
                printlnOut("InvalidAlgorithmParameterException");
            }
        } catch (NoSuchAlgorithmException e) {
            printlnOut("NoSuchAlgorithmException");
        } catch (NoSuchPaddingException e) {
            printlnOut("NoSuchPaddingException");
        } catch (InvalidKeyException e) {
            printlnOut("InvalidKeyException");
        }


    }
    
    public byte[] getHMAC(String message, Config config){
    	File f = new File(config.getString("hmac.key"));
        Key secret;
    	if (f == null) {
            printlnOut("File not found for shared secret key");
            return null;
        }
        try {
            secret = Keys.readSecretKey(f);
        } catch (IOException e) {
            printlnOut("IOException at Keys.readSecretKey(f) ");
            return null;
        }
        System.out.println("this is the message - " + message);
        try{
        	Mac hMac = Mac.getInstance("HmacSHA256");
        	hMac.init(secret);
        	hMac.update(message.getBytes());
        	byte[] hash = hMac.doFinal();
        	return hash;
        } catch (NoSuchAlgorithmException e){
        	printlnOut("NoSuchAlgorithmException");
        } catch (InvalidKeyException e){
        	printlnOut("InvalidKeyException");
        }
        return null;
    }
    
    public void printlnOut(String msg) {
        channel.printlnOut(msg);
    }

    public void exit(int i) {
        channel.exit(i);
    }

}
