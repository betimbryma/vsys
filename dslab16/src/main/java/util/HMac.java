package util;

import org.bouncycastle.util.encoders.Base64;
import javax.crypto.*;
import java.io.File;
import java.io.IOException;
import java.security.*;

public final class HMac {

    public static byte[] getHMAC(String message, Config config){
    	File f = new File(config.getString("hmac.key"));
        Key secret;
    	if (f == null) {
    		System.out.println("File not found for shared secret key");
            return null;
        }
        try {
            secret = Keys.readSecretKey(f);
        } catch (IOException e) {
        	System.out.println("IOException at Keys.readSecretKey(f) ");
            return null;
        }
//        System.out.println(message);
        try{
        	Mac hMac = Mac.getInstance("HmacSHA256");
        	hMac.init(secret);
        	hMac.update(message.getBytes());
        	byte[] hash = hMac.doFinal();
        	byte[] base64hmac = Base64.encode(hash);
        	return base64hmac;
        } catch (NoSuchAlgorithmException e){
        	System.out.println("NoSuchAlgorithmException");
        } catch (InvalidKeyException e){
        	System.out.println("InvalidKeyException");
        }
        return null;
    }
    
    public static byte[] getWrongHMAC(String message, Config config){
    	File f = new File(config.getString("wrongHmac.key"));
        Key secret;
    	if (f == null) {
    		System.out.println("File not found for shared secret key");
            return null;
        }
        try {
            secret = Keys.readSecretKey(f);
        } catch (IOException e) {
        	System.out.println("IOException at Keys.readSecretKey(f) ");
            return null;
        }
  //      System.out.println(message);
        try{
        	Mac hMac = Mac.getInstance("HmacSHA256");
        	hMac.init(secret);
        	hMac.update(message.getBytes());
        	byte[] hash = hMac.doFinal();
        	byte[] base64hmac = Base64.encode(hash);
        	return base64hmac;
        } catch (NoSuchAlgorithmException e){
        	System.out.println("NoSuchAlgorithmException");
        } catch (InvalidKeyException e){
        	System.out.println("InvalidKeyException");
        }
        return null;
    }
}
