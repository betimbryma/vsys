package util;

import java.io.IOException;

/**
 * Created by eni on 08.01.2017.
 */
public interface Channel {
    String authenticateServer( Config config) throws IOException;
    String authenticateClient(String username, Config config) throws IOException;
    boolean isPrintOpen();
    String read() throws IOException;
    void write(String string);
    byte[] getHMAC(String message, Config config);
    void printlnOut(String msg);
    void exit(int i);
}