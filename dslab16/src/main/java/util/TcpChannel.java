package util;

import client.Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * Created by eni on 08.01.2017.
 */
public class TcpChannel implements Channel{
    private BufferedReader bufferedReader;
    private Client client;
    private PrintWriter printWriter;
    private PrintStream userResponseStream;

    public TcpChannel(BufferedReader bufferedReader,PrintStream userResponseStream,PrintWriter printWriter){
        this.bufferedReader=bufferedReader;
        this.client=client;
        this.printWriter=printWriter;
        this.userResponseStream=userResponseStream;

    }


    public String authenticateClient(String username, Config config){
        return null;
    }
    public boolean isPrintOpen(){
        if(printWriter!=null)
            return true;
        return false;

    }

    public String read() throws IOException {
        return bufferedReader.readLine();
    }

    public void write(String string){
        printWriter.println(string);
    }

    public byte[] getHMAC(String message, Config config){
    	return null;
    }
    
    public void printlnOut(String msg){
        userResponseStream.println(msg);
    }

    public void exit(int i) {
        if(i==1){
            //  client.exit();
        }

    }



    public String authenticateServer(Config config) throws IOException{
        return null;
    }
}
