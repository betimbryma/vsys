package chatserver;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import nameserver.INameserverForChatserver;
import nameserver.exceptions.AlreadyRegisteredException;
import nameserver.exceptions.InvalidDomainException;
import util.*;
import util.Base64Channel;

public class ListenerThread implements Runnable {
	private User user = null;
	private Socket socket;
	private ServerPool serverpool;
	private Config config;
	private BufferedReader reader;
	private PrintWriter writer;
	private INameserverForChatserver iNameserverForChatserver;
	private boolean authenticated;
	private PrintStream userResponseStream;
	private Channel channel;
	
	public ListenerThread(Socket socket,ServerPool serverpool,Config config, INameserverForChatserver iNameserverForChatserver,PrintStream userResponseStream){
		this.socket=socket;
		this.serverpool=serverpool;
		this.config=config;
		this.iNameserverForChatserver = iNameserverForChatserver;
		this.authenticated=false;
		this.userResponseStream=userResponseStream;

	}
	@Override
	public void run() {

			try {

				reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				writer = new PrintWriter(socket.getOutputStream(),true);
				channel = new Base64Channel(new TcpChannel(reader,userResponseStream,writer));

				boolean notNull=true;
				String request;
				while (notNull) {
					if (!authenticated) {
						String username=channel.authenticateServer(config);


						if((username)!=null){
							synchronized (serverpool) {
								if (serverpool.isUserLoggedIn(username)) {
									sendMessage("user already logged in on another client..");
									continue;
								} else {
									user = new User();
									user.setName(username);
									user.setPassword("");
									serverpool.insertUser(user, this);
									sendMessage("Successfully Authenticated");
									authenticated=true;
									continue;
								}
							}
						}else{
							continue;
						}
					} else {
						if((request = channel.read()) == null){
							notNull=true;
							continue;
						}
						String[] requestArray = request.split(" ");
						if (requestArray.length < 1) {
							//do something with it ---------------------------------------------
							continue;
						}

						//start login
						if (requestArray[0].equals("!login")) {
							if (user != null) {
								sendMessage("reply" + "Already logged in.");
								continue;
							} else {
								if (requestArray.length == 3) {
									this.config = new Config("user");
									Set<String> s = config.listKeys();
									String username = requestArray[1] + ".password";
									if (s.contains(username)) {
										String i = config.getString(username);
										if (i.equals(requestArray[2])) {
											if (serverpool.isUserLoggedIn(requestArray[1])) {
												sendMessage("reply" + "user already logged in on another client..");
												continue;
											} else {
												user = new User();
												user.setName(requestArray[1]);
												user.setPassword(requestArray[2]);
												serverpool.insertUser(user, this);
												sendMessage("reply" + "Successfully logged in.");
												continue;
											}
										}
									}

								}
							}
							sendMessage("reply" + "Wrong username or password.");
							continue;
						}//--end !login

						//--start logout
						if (requestArray[0].equals("!logout")) {
							if (user != null) {
								serverpool.removeUser(user);
								user = null;
								sendMessage("reply" + "Successfully logged out.");
								authenticated=false;
								continue;
							} else {
								sendMessage("reply" + "Not logged in.");
								continue;
							}
						}//--end logout

						//--start send
						if (requestArray[0].equals("!send") && user != null) {
							String[] requestArray2 = request.split("!send|");
							String message = "";
							for (int i = 1; i < requestArray2.length; i++) {
								message += requestArray2[i];
							}
							synchronized (serverpool) {
								serverpool.sendMessageToAll(user, message);
							}
							sendMessage("reply" + "Success");
							continue;
						}//end send
						//--start register
						if (requestArray[0].equals("!register") && user != null) {
							//-------------------------------------------------------check register
							if (requestArray.length == 2) {
								try {
									iNameserverForChatserver.registerUser(user.getName(), requestArray[1]);
									sendMessage("reply" + "Successfully registered address for " + user.getName() + ".");
								} catch (InvalidDomainException i){
									sendMessage("reply" + "Sorry couldn't register " + i.getMessage() + ".");
								} catch (AlreadyRegisteredException e) {
									sendMessage("reply" + "Sorry couldn't register " + e.getMessage() + ".");
								}
								continue;
							}
						}//end register
						//--start lookup
						if (requestArray[0].equals("!lookup") && user != null) {
							if (requestArray.length == 2) {
								String address = serverpool.lookup(requestArray[1]);
								if (address == null) {
									sendMessage("reply" + "Wrong username or user not registered.");
								} else {
									sendMessage("reply" + address);
								}
								continue;
							}
						}//end lookup


						if (requestArray[0].equals("!exit") && user != null) {
							if (serverpool.containUser(user.getName()))
								serverpool.removeUser(user);
							break;
						}//end lookup

						sendMessage("reply" + "Not logged in.");
					}
				}
			} catch (IOException e) {
				userResponseStream.println("Error occurred while waiting for/communicating with client: "+ e.getMessage());
			} finally {
				exit();

			}

		
	}
	public  synchronized  void sendMessage(String message){//---------------------------------- Semaphore use
		channel.write(message);
	}
	public  void exit() {//---------------------------------- //Server is exiting

		if(user!=null)
			if(serverpool.containUser(user.getName()))
				serverpool.removeUser(user);
		if(writer!=null)
			writer.close();
		if(socket!=null&&!socket.isClosed()){
			try{
				socket.close();
			}catch(IOException e){
				
			}
		}
		if(reader!=null)
			try{
				reader.close();
			}catch(IOException e){
				
			}
		
	
	}
}
