package chatserver;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.rmi.RemoteException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import nameserver.INameserver;
import nameserver.INameserverForChatserver;
import util.Config;

public class ServerPool implements Runnable {

	private String componentName;
	private Config config;
	private InputStream userRequestStream;
	private PrintStream userResponseStream;
	
	
	private ServerSocket serverSocket;
	private ExecutorService chatPool;
	private Chatserver chatserver;
	private HashMap<String,User> mapNameUser;
	private HashMap<User,ListenerThread> mapUserListenerThread;
	private Set<User> usersSet;
	private Set<ListenerThread> listenerThread;
	private INameserverForChatserver iNameserverForChatserver;
	
	public ServerPool(String componentName,Config config,Chatserver chatserver, INameserverForChatserver iNameserverForChatserver,PrintStream userResponseStream){
		
		this.componentName=componentName;
		this.config = config;
		this.chatserver=chatserver;
		this.iNameserverForChatserver = iNameserverForChatserver;
		mapNameUser=new HashMap<>();
		mapUserListenerThread=new HashMap<>();
		usersSet=new HashSet<>();
		chatPool = Executors.newCachedThreadPool();
		listenerThread = new HashSet<>();
		this.userResponseStream=userResponseStream;
	}

	public void run(){

		try {
			serverSocket = new ServerSocket(config.getInt("tcp.port"));
			// handle incoming connections from client in a separate thread
			while(true){
				ListenerThread a = new ListenerThread(serverSocket.accept(),this,config, iNameserverForChatserver,userResponseStream);
				listenerThread.add(a);
				chatPool.execute(a);
			}
			
		} catch (IOException e) {
			
		} catch (Exception e){
			
		}finally{
			exit();		
		}
		
		
		
	}
	
	public boolean isUserLoggedIn(String user){
		return mapNameUser.containsKey(user);
	}

	public synchronized  void insertUser(User user,ListenerThread lt){
		this.chatserver.setOnlineUser(user.getName());
		mapNameUser.put(user.getName(), user);
		mapUserListenerThread.put(user,lt);
		usersSet.add(user);
	}
	public boolean containUser(String  user){
		return mapNameUser.containsKey(user);
	}
	public synchronized void removeUser(User user){
		this.chatserver.setOfflineUser(user.getName());
		mapNameUser.remove(user.getName());
		mapUserListenerThread.remove(user);
		usersSet.remove(user);
	}
	public synchronized  void sendMessageToAll(User user,String message){

		for(User s: usersSet){
			if(!s.equals(user)){
				ListenerThread t = mapUserListenerThread.get(s);
				t.sendMessage("group"+user.getName()+":"+message);
				
			}
		}

	}
	public synchronized  String lookup(String s){
		System.out.println(s);
		if(mapNameUser.containsKey(s)){
			List<String> domains = Arrays.asList(s.split("\\."));
			int size = domains.size();
			INameserverForChatserver domain = iNameserverForChatserver;
			INameserverForChatserver current = iNameserverForChatserver;
			INameserverForChatserver iChat = iNameserverForChatserver;
			try {
				for (int i = size - 1; i >= 1; i--) {
					current = domain.getNameserver(domains.get(i));
					domain = current;
				}
				return current.lookup(domains.get(0));
			} catch (RemoteException e) {
				e.printStackTrace();
			}

		}
		return null;
	}
	
	public Set<User> getOnlineUsers(){
		return usersSet;
	}
	
	public void exit(){ //exit from server;
		for(ListenerThread t: listenerThread){
			t.exit();
		}
		chatPool.shutdown();
		if(serverSocket!=null&&!serverSocket.isClosed()){
			try {
				serverSocket.close();
			} catch (IOException e) {
				//cant catch
			}
		}

	}
	
	
}
