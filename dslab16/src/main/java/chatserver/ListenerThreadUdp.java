package chatserver;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

import util.Config;

public class ListenerThreadUdp implements Runnable{
	private DatagramPacket packet;
	private DatagramSocket datagramSocket;
	private ServerPool serverpool;
	
	
	public ListenerThreadUdp(DatagramPacket packet,DatagramSocket datagramSocket, ServerPool serverpool){
		this.packet=packet;
		this.serverpool=serverpool;
		this.datagramSocket=datagramSocket;
	}
	
	public void run(){

		String receive = new String(packet.getData());
		String response="Online users:\n";
		
		ArrayList<String> us = new ArrayList<>();
		Set<User> users=serverpool.getOnlineUsers();
		for(User u:users){
			us.add(u.getName());
		}
		Collections.sort(us);
		for(String u:us){
			response+="* "+u+"\n";
		}
		byte[] buffer=response.getBytes();
		DatagramPacket packet2=new DatagramPacket(buffer,buffer.length, packet.getAddress(),packet.getPort());
		try {
			datagramSocket.send(packet2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//nothing to do
		}
		
	}
	
}
