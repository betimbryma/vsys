package chatserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import nameserver.INameserverForChatserver;
import util.Config;

public class Chatserver implements IChatserverCli, Runnable {

	private String componentName;
	private Config config;
	private Config userConfig;
	private InputStream userRequestStream;
	private PrintStream userResponseStream;
	private HashMap<String,String> users;
	private ArrayList<String> s;
	private ServerPool serverPool;
	private ServerPoolUdp serverPoolUdp;
	private BufferedReader reader;
	private INameserverForChatserver iNameserverForChatserver;
	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public Chatserver(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream) {
		this.componentName = componentName;
		this.config = config;
		this.userRequestStream = userRequestStream;
		this.userResponseStream = userResponseStream;
		this.userConfig = new Config("user");
		Registry registry = null;
		try {
			registry = LocateRegistry.getRegistry(config.getString("registry.host"), config.getInt("registry.port"));
			this.iNameserverForChatserver = (INameserverForChatserver) registry.lookup(config.getString("root_id"));
		} catch (RemoteException e) {
			userResponseStream.println("Nameserver needs to be started before you can start the chatserver");

		} catch (NotBoundException e) {
			userResponseStream.println("Not Bound Exception");
		}


	}
	
	@Override
	public void run() {

		users =new HashMap<>();
		this.s= new ArrayList<>();
		Set<String> s = userConfig.listKeys();
		for(String u: s){
			u=u.substring(0,u.length()-9);
			users.put(u, "offline");
			this.s.add(u);
		}
		Collections.sort(this.s);
		
		
		
		
		serverPool = new ServerPool(componentName,config,this, iNameserverForChatserver,userResponseStream);
		serverPoolUdp = new ServerPoolUdp( config, serverPool);
		new Thread(serverPool).start();
		new Thread(serverPoolUdp).start();
		
		
		
		try {
				// read commands from the console

			 reader = new BufferedReader(new InputStreamReader(userRequestStream));
			while(true){
				String msg= reader.readLine();
				if(msg.equals("!users")){
					userResponseStream.println(users());
				}else if(msg.equals("!exit")){
					userResponseStream.println(exit());
				}
				
			}	
		} catch (IOException e) {
			// IOException from System.in is very very unlikely (or impossible)
			try {
				exit();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				//cant catch
			}
		}
	}

	public void setOnlineUser(String user){
		users.put(user, "online");
	}
	public void setOfflineUser(String user){
		users.put(user, "offline");
	}
	@Override
	public String users() throws IOException {
		// TODO Auto-generated method stub
		String message = "";
		int i =1;
		for(String ss: s){
			message += i++ +". " + ss +" "+ users.get(ss) +"\n"; 
		}
		return message;
	}

	@Override
	public String exit() throws IOException {
		// TODO Auto-generated method stub

		if(serverPool!=null)
			serverPool.exit();
		if(serverPoolUdp!=null)
			serverPoolUdp.exit();
		if(reader!=null)
			reader.close();
		return "Server exit successful";
	}

	/**
	 * @param args
	 *            the first argument is the name of the {@link Chatserver}
	 *            component
	 */
	public static void main(String[] args) {

		Chatserver chatserver = new Chatserver(args[0],
				new Config("chatserver"), System.in, System.out);

		new Thread(chatserver).start();
	}

}
