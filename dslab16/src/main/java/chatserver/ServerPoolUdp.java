package chatserver;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import util.Config;

public class ServerPoolUdp implements Runnable {

	private Config config;
	private DatagramSocket datagramSocket;
	private ExecutorService chatPool;
	private ServerPool serverpool;
	
	public ServerPoolUdp(Config config,ServerPool serverpool) {
		this.config = config;
		this.serverpool = serverpool;
		chatPool = Executors.newCachedThreadPool();
	}
	
	public void run(){
		try {
			int port = config.getInt("udp.port");
			datagramSocket = new DatagramSocket(port);
			byte[] buf = new byte[100];
			while(true){
				DatagramPacket packet = new DatagramPacket(buf,buf.length);
				datagramSocket.receive(packet);
				chatPool.execute(new ListenerThreadUdp(packet,datagramSocket,serverpool));
			}
			
			
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}finally{
			exit();
		}
	}
	
	public void exit(){
		if (datagramSocket != null && !datagramSocket.isClosed())
			datagramSocket.close();
		if(chatPool!=null)
			chatPool.shutdown();
	}
	
}
