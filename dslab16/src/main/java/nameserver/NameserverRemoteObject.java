package nameserver;

import nameserver.exceptions.AlreadyRegisteredException;
import nameserver.exceptions.InvalidDomainException;

import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Betim on 24.12.2016.
 */
public class NameserverRemoteObject implements INameserver {

    private boolean root;
    private ConcurrentHashMap<String, INameserver> subdomains;
    private ConcurrentHashMap<String, String> userAddresses;
    private boolean alive;
    private Nameserver nameserverParent;

    public NameserverRemoteObject(Nameserver nameserverParent, ConcurrentHashMap<String, INameserver> subdomains, boolean root) {

        this.nameserverParent = nameserverParent;
        this.alive = true;
        this.root = root;
        this.subdomains = subdomains;
        this.userAddresses = new ConcurrentHashMap<>();

    }

    @Override
    public void registerUser(String username, String address) throws RemoteException, AlreadyRegisteredException, InvalidDomainException {

        List<String> userName = Arrays.asList(username.split("\\."));
        int size = userName.size();
        String zone = userName.get(size - 1);

        if(size == 1) {
            if(root)
                throw new InvalidDomainException("Sorry, but this is an invalid username");
            userAddresses.put(username, address);
            nameserverParent.addUsername(username + " " + address);
        }
        else if(size > 1){

            if(subdomains.containsKey(zone)){

                StringBuilder stringBuilder = new StringBuilder();
                INameserver iNameserver = subdomains.get(zone);

                for(int i =0; i<size -1; i++) {
                    if(i == 0)
                        stringBuilder.append(userName.get(i));
                    else
                        stringBuilder.append("."+userName.get(i));
                }

                iNameserver.registerUser(stringBuilder.toString(), address);
            } else
                throw new InvalidDomainException("Sorry, it looks like "+userName.get(size - 1)+" is an invalid domain name.");

        } else {
            throw new InvalidDomainException("Sorry, it looks like this is an invalid username. Try again.");
        }
    }

    @Override
    public INameserverForChatserver getNameserver(String zone) throws RemoteException {
        return subdomains.get(zone);
    }

    @Override
    public String lookup(String username) throws RemoteException {
        return this.userAddresses.get(username);
    }

    @Override
    public void registerNameserver(String domain, INameserver nameserver, INameserverForChatserver nameserverForChatserver) throws RemoteException, AlreadyRegisteredException, InvalidDomainException {

        List<String> domainName = Arrays.asList(domain.split("\\."));
        int size = domainName.size();
        String zone = domainName.get(size - 1);
        if(size == 1){

            if(subdomains.containsKey(zone)) {
                boolean isAlive = false;
                try{
                    isAlive = subdomains.get(zone).isAlive();
                } catch (ConnectException c){
                    //continue
                }
                if(!isAlive) {
                    subdomains.put(zone, nameserver);
                    nameserverParent.addDomain(zone);                }
                else
                    throw new AlreadyRegisteredException("Sorry, it looks like this domain is already registered.");
            }
            else {
                subdomains.put(zone, nameserver);
                nameserverParent.addDomain(zone);
            }


        } else if(size > 1){

            if(subdomains.containsKey(zone)){
                INameserver iNameserver = subdomains.get(zone);
                StringBuilder stringBuilder = new StringBuilder();

                for(int i =0; i<size -1; i++) {
                    if(i == 0)
                        stringBuilder.append(domainName.get(i));
                    else
                        stringBuilder.append("."+domainName.get(i));
                }

                iNameserver.registerNameserver(stringBuilder.toString(), nameserver, nameserverForChatserver);
            } else
                throw new InvalidDomainException("Sorry, it looks like "+domainName.get(size - 1)+" is an invalid domain name.");

        } else {
            throw new InvalidDomainException("Sorry, it looks like this is an invalid domain name. Try again.");
        }

    }

    @Override
    public void shutdown() throws RemoteException {
        this.alive = false;
    }

    @Override
    public boolean isAlive() throws RemoteException {
        return this.alive;
    }

}
