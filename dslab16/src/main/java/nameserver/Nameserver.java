package nameserver;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.SocketException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import cli.Command;
import cli.Shell;
import nameserver.exceptions.AlreadyRegisteredException;
import nameserver.exceptions.InvalidDomainException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import util.Config;

import javax.print.DocFlavor;

/**
 * Please note that this class is not needed for Lab 1, but will later be used
 * in Lab 2. Hence, you do not have to implement it for the first submission.
 */
public class Nameserver implements INameserverCli, Runnable {

	private String componentName;
	private Config config;
	private InputStream userRequestStream;
	private PrintStream userResponseStream;
	private Shell shell;
	private ConcurrentHashMap<String, INameserver> children;
	private static Log LOGGER = LogFactory.getLog(Nameserver.class);
	private Registry registry;
	private INameserver stub;
	private final INameserver remoteObject;
	private List<String> domains;
	private List<String> usernames;
	private boolean root;
	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */

	public Nameserver(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream){

		this.componentName = componentName;
		this.config = config;
		this.userRequestStream = userRequestStream;
		this.userResponseStream = userResponseStream;
		this.shell = new Shell(componentName, userRequestStream, userResponseStream);
		shell.register(this);
		this.children = new ConcurrentHashMap<>();
		this.domains = new ArrayList<>();
		this.usernames = new ArrayList<>();
		if(config.listKeys().contains("domain")){
			//it is not root

            this.remoteObject = new NameserverRemoteObject(this, children, false);

            try {
                this.stub = (INameserver) UnicastRemoteObject.exportObject(remoteObject, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
			try {
				this.registry = LocateRegistry.getRegistry(config.getString("registry.host"),config.getInt("registry.port"));
				INameserver remote = (INameserver) registry.lookup(config.getString("root_id"));

                remote.registerNameserver(config.getString("domain"), stub, stub);

			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (InvalidDomainException e) {
				e.printStackTrace();
			} catch (NotBoundException e) {
				e.printStackTrace();
			} catch (AlreadyRegisteredException e) {
				e.printStackTrace();
			}
		} else {

            this.remoteObject = new NameserverRemoteObject(this, children, true);

            try {
                this.stub = (INameserver) UnicastRemoteObject.exportObject(remoteObject, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            this.root = true;
			try {
				this.registry = LocateRegistry.createRegistry(config.getInt("registry.port"));
				registry.bind(config.getString("root_id"), stub);

			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (AlreadyBoundException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void run() {
		new Thread(shell).run();
	}

	@Override
	@Command("!nameservers")
	public String nameservers() throws IOException {

		StringBuilder stringBuilder = new StringBuilder();

		for(String string : children.keySet()) {
		    try {
                if (children.get(string).isAlive())
                    stringBuilder.append(string + System.lineSeparator());
            } catch (RemoteException e){
		        //continue
            }
        }

		return stringBuilder.toString();
	}

	@Override
	@Command("!addresses")
	public String addresses() throws IOException {
		StringBuilder stringBuilder = new StringBuilder();
		for(String string : usernames)
			stringBuilder.append(string + System.lineSeparator());
		return stringBuilder.toString();
	}

	@Override
    @Command("!exit")
	public String exit() throws IOException {

		if(root) {

			UnicastRemoteObject.unexportObject(remoteObject, true);
			try {
				registry.unbind(config.getString("root_id"));
			} catch (NotBoundException e) {
				e.printStackTrace();
			}
		}
		else
            UnicastRemoteObject.unexportObject(remoteObject, true);

        shell.close();
		return null;
	}



	public void addDomain(String domain){
		domains.add(domain);
	}
	public void addUsername(String username){ usernames.add(username);}

	/**
	 * @param args
	 *            the first argument is the name of the {@link Nameserver}
	 *            component
	 */
	public static void main(String[] args) {
		Nameserver nameserver = new Nameserver(args[0], new Config(args[0]),
				System.in, System.out);
		new Thread(nameserver).start();
	}

}
